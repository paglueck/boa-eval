import numpy as np
from scipy.stats.kde import gaussian_kde

'''
Implementation of the Bag of Actions approach

Summarized depiction of the Ablauf:
- Create Bags of Actions from training data logs
- Calculate random vars for each Bag
- Estimate probability density function with random vars
- Calculate threshold with pdf and sensitivity factor alpha
'''


'''
Constant vars - action names, actions array, sensitivity for the detection
'''
CONST_ACTION_NAME_LOGIN = "LOGIN"
CONST_ACTION_NAME_LOGOUT = "LOGOUT"
CONST_ACTION_NAME_PWCHANGE = "PW-CHANGE"
CONST_ACTION_NAME_LOGINFAIL = "LOGINFAIL"
CONST_ACTION_NAME_MAILCHANGE = "EMAIL-CHANGE"
CONST_ACTION_NAME_PWRESET = "PW-RESET"

CONST_ACTIONS = [CONST_ACTION_NAME_LOGIN,
                 CONST_ACTION_NAME_LOGOUT,
                 CONST_ACTION_NAME_PWCHANGE,
                 CONST_ACTION_NAME_LOGINFAIL,
                 CONST_ACTION_NAME_MAILCHANGE,
                 CONST_ACTION_NAME_PWRESET]

CONST_LOG_DETAIL_SEPARATOR = " "


def process_logs(logs):
    """
    Preprocessing of log strings into raw bags of actions (raw meaning that they're not Numpy vectors yet)

    Parameters
    ----------
    :param logs: Array containing 0..n arrays (representing individual logs) with 1..n log entry strings ([[],[],..,[]])

    :return: Returns an array of arrays, each array in the general array contains the number of occurrences for the log
    actions. The number of arrays in the general array corresponds to the number of logs in the input array ([],..,[])
    """
    raw_bags = []
    for log in logs:
        raw_bag = []
        for entry in log:
            entry_splitted = entry.split(CONST_LOG_DETAIL_SEPARATOR)
            raw_bag.append(entry_splitted[6])
        raw_bags.append([float(raw_bag.count(CONST_ACTION_NAME_LOGIN)),
                         float(raw_bag.count(CONST_ACTION_NAME_LOGOUT)),
                         float(raw_bag.count(CONST_ACTION_NAME_PWCHANGE)),
                         float(raw_bag.count(CONST_ACTION_NAME_LOGINFAIL)),
                         float(raw_bag.count(CONST_ACTION_NAME_MAILCHANGE)),
                         float(raw_bag.count(CONST_ACTION_NAME_PWRESET))])
    return raw_bags


def detect(training, prediction, alpha):
    """
    Training and prediction with Bag of Actions approach

    Parameters
    ----------
    :param training: Raw user logs for training as array of string arrays. ([[string log entry,..],..[])
    :param prediction: Raw user logs for prediction as array of string arrays. ([[string log entry,..],..[])

    Return
    -------
    :return: Returns an array containing a bool value at the first position of True if an anomaly was found and False
    if none was found. At the second position the method returns a prediction which position in the actions array was
    responsible for the cause of that anomaly (0 = Logins, 1 = Logouts, etc.)
    """

    '''
    Training
    '''

    '''
    Step 1 - Training: Get Bags of Actions and sensitivity factor alpha
    '''
    training_bags = process_logs(training)
    training_bag_vectors = list(map(lambda x: np.array(x), training_bags))
    print "CALCULATED BAGS"
    for bag in training_bags: print bag
    '''
    Step 2 - Training: Calculate random variables x_n
    '''
    big_n = len(training_bag_vectors)
    sum_of_distances = []
    for bag_i in training_bag_vectors:
        sum_of_distance = 0
        for bag_j in training_bag_vectors:
            sum_of_distance += np.absolute(bag_i - bag_j)
        sum_of_distances.append(sum_of_distance)
    #print sum_of_distances
    random_vars = list(map(lambda y: np.sum(y)/big_n, sum_of_distances))
    random_vars_vec = np.array(random_vars)
    # print "CALCULATED RANDOM VARS"

    '''
    Step 3 - Training: Estimate PDF
    '''
    KDEpdf = gaussian_kde(random_vars_vec)
    # print "ESTIMATED PDF"

    x = np.linspace(-1.5, 1.5, 1500)

    """
    --Print averages--
    print "printing p(x) of averages: "
    out = ""
    for x in random_vars:
        out += str(KDEpdf(x))
    print out
    """


    '''
    Step 4 - Training: Calculate Threshold theta
    '''
    sum_of_p_xs = 0
    for x in random_vars:
        sum_of_p_xs += KDEpdf(x)
    theta = (sum_of_p_xs / big_n) * alpha
    print "CALCULATED THRESHOLD"
    print "THRESHOLD: " + str(theta)

    '''
    Prediction
    '''

    '''
    Step 5 - Prediction: Calculate Bags for prediction logs
    '''
    prediction_bags = []
    if prediction is not None and len(prediction) > 0:
        prediction_bags = process_logs(prediction)
    else:
        prediction_bags = process_logs(training)
    prediction_bag_vectors = list(map(lambda x: np.array(x), prediction_bags))
    print "CALCULATED PREDICTION BAGS"
    for bag in prediction_bags: print bag


    '''
    Step 6 - Prediction: Calculated random vars for prediction bags
    '''
    prediction_sum_of_distances= []
    for bag_u in prediction_bag_vectors:
        sum_of_distance = 0
        for bag_i in training_bag_vectors:
            sum_of_distance += np.absolute(bag_i - bag_u)
        prediction_sum_of_distances.append(sum_of_distance)

    prediction_random_vars = list(map(lambda y: np.sum(y)/big_n, prediction_sum_of_distances))
    #print "CALCULATED PREDICTION RANDOM VARS"

    '''
    Step 7 - Prediction: Try to give a prediction for the cause for a potential anomaly
    '''
    maxs = 0
    predsize = len(prediction_sum_of_distances[0])
    for i in range(predsize):
        if prediction_sum_of_distances[0][i] >= maxs:
            maxs = prediction_sum_of_distances[0][i]
    poss = []
    for i in range(predsize):
        if prediction_sum_of_distances[0][i] == maxs:
            poss.append(i)

    '''
    Step 8 - Prediction: Compare values of the prediction bags applied to the PDF with the the threshold
    '''
    print "LOOKING FOR ANOMALIES:"
    anom_found = False
    for i in prediction_random_vars:
        if KDEpdf(i) < theta:
            print "anomaly! (" + str(KDEpdf(i)) + ") in bag " + str(prediction_random_vars.index(i))
            anom_found = True
        else:
            print "no anomaly! ( " + str(KDEpdf(i)) + ")"

    '''
    Step 9 - Return result
    '''
    return [anom_found, poss]