import json
from nameko.rpc import rpc, RpcProxy
from . import bagofactions


'''
Constant vars - Names of the other services
'''
CONST_SERVICE_NAME_AD = 'detection_service'
CONST_SERVICE_NAME_AD_STORAGE = 'anomaly_storage_service'
CONST_SERVICE_NAME_HUB = 'hub_service'
CONST_SERVICE_NAME_GEN = 'generator_service'


class DetectionService:
    """
    Microservice offering the anomaly detection functionality
    """

    '''
    Name the microservice reacts to, when listening for calls
    '''
    name = CONST_SERVICE_NAME_AD

    '''
    RPC proxies the anomaly detection service needs to call other services it depends on
    '''
    gen = RpcProxy(CONST_SERVICE_NAME_GEN)
    hub = RpcProxy(CONST_SERVICE_NAME_HUB)
    das = RpcProxy(CONST_SERVICE_NAME_AD_STORAGE)

    @rpc
    def detect_anomalies_with_ubh(self, uid, ubh, log_to_check):
        """
        Anomaly detection RPC endpoint - real functionality

        Parameters
        ----------
        :param uid: ID of the user whose logs get checked for anomalies
        :param ubh: The historic behavior of the user
        :param log_to_check: The new behavior of the user which gets checked for deviation with the anomaly detection

        :return: Returns True if an anomaly was detected and False if none was detected
        """
        result = bagofactions.detect(ubh, [log_to_check])
        if result[0]:
            day = log_to_check[0].split(" ")[0].split("T")[0]
            cause = "Causes: ["
            for max in result[1]:
                print max
                if max == 0:
                    cause += " Logins "
                elif max == 1:
                    cause += " Logouts "
                elif max == 2:
                    cause += " PW-Changes "
                elif max == 3:
                    cause += " Loginfails "
                else:
                    cause += " generic "
            cause += "]"
            self.das.store_anomaly(day, uid, cause)
            return True
        else:
            return False

    @rpc
    def detect_anomalies(self, training, prediction, alpha):
        """
        Anomaly detection RPC endpoint - for evaluation/testing purposes

        Parameters
        ----------
        :param training: Raw user logs for training as array of string arrays. ([[string log entry,..],..[])
        :param prediction: Raw user logs for prediction as array of string arrays. ([[string log entry,..],..[])

        :return: Passes through the returned array of the detect method of the bagofactions.py module
        """
        return bagofactions.detect(training, prediction, alpha)
