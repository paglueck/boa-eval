from nameko.rpc import rpc, RpcProxy

'''
Constant vars - Necessary microservice names
'''
CONST_SERVICE_NAME = 'gateway_service'
CONST_SERVICE_NAME_AD = 'detection_service'
CONST_SERVICE_NAME_GEN = 'generator_service'

CONST_AD_SENSITIVITY = 0.5

CONST_GRANULARITY_DAY = 'DAY'
CONST_GRANULARITY_HALFDAY = 'HALFDAY'
CONST_GRANULARITY_QUARTERDAY = 'QUARTERDAY'


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
             for i in range(wanted_parts) ]


def convert_into_granularity(logs, granularity):
    new_logs = []
    for log in logs:
        for splitted_log in split_list(log, granularity):
            new_logs.append(splitted_log)
    return new_logs


class Gateway:
    """
    Microservice offering the anomaly detection functionality
    """

    name = CONST_SERVICE_NAME
    gen = RpcProxy(CONST_SERVICE_NAME_GEN)
    detect = RpcProxy(CONST_SERVICE_NAME_AD)

    @rpc
    def generate_behavior_and_detect(self, args):
        """
        Generate behavior and detect anomalies

        Parameters
        ----------
        :param args: Contains 'granularity', 'behavior' and 'alpha'

        :return: Returns a tuple of the generated logs and the result of the detection
        """

        # granularity determines how many entries are used for one bag, e.g.: day, week, month or plain number
        # standard is a day (at least three bags are necessary for the bag of actions approach)
        granularity = args['granularity']

        granularity_parts = 1
        if granularity == CONST_GRANULARITY_HALFDAY:
            granularity_parts = 2
        elif granularity == CONST_GRANULARITY_QUARTERDAY:
            granularity_parts = 4

        # Parameter for detection (alpha = sensitivity, higher = more anomalies, lower = less anomalies)
        alpha = args['alpha']

        '''
        Training data
        '''

        # Parameters for the generator to generate behavior
        behavior_train = args['training_behavior']

        # Generate user behavior
        training_logs = self.gen.generate_with_args(behavior_train["days"],
                                                    behavior_train["actions_amount"],
                                                    behavior_train["action_distribution"],
                                                    behavior_train["anomalies"],
                                                    behavior_train["day_offset"],
                                                    behavior_train["minutes_to_distribute"],
                                                    behavior_train['deviation_lower'],
                                                    behavior_train['deviation_upper'])

        final_training_logs = convert_into_granularity(training_logs, granularity_parts)

        '''
        Prediction data
        '''

        final_prediction_logs = []

        if 'predict_behavior' in args.keys():

            # Parameters for the generator to generate behavior
            behavior_predict = args['predict_behavior']

            # Generate user behavior
            prediction_logs = self.gen.generate_with_args(behavior_predict["days"],
                                                          behavior_predict["actions_amount"],
                                                          behavior_predict["action_distribution"],
                                                          behavior_predict["anomalies"],
                                                          behavior_predict["day_offset"],
                                                          behavior_predict["minutes_to_distribute"],
                                                          behavior_predict['deviation_lower'],
                                                          behavior_predict['deviation_upper'])

            final_prediction_logs = convert_into_granularity(prediction_logs, granularity_parts)

        # Detect anomalies
        detection_result = self.detect.detect_anomalies(final_training_logs, final_prediction_logs, CONST_AD_SENSITIVITY)

        return final_training_logs, final_prediction_logs, detection_result
