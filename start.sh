#!/bin/bash

if [ -d ./boa_eval/ ]
then
    echo "Virtual environment exists already... continue with microservice start"
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run generator.generator.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run detection.detection.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run gateway.gateway.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run evaluation.evaluation.service" in tab 1 of window 1' -e 'delay 1'
else
    echo "Virtual environment not existing yet... creating virtualenv and installing required packages"
    virtualenv --no-site-packages boa_eval
    osascript -e 'delay 3'
    source boa_eval/bin/activate
    pip install -r requirements.txt
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run generator.generator.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run detection.detection.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run gateway.gateway.service" in tab 1 of window 1' -e 'delay 1'
    osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' -e 'delay 1' -e 'tell application "Terminal" to do script "source boa_eval/bin/activate" in tab 1 of window 1' -e 'tell application "Terminal" to do script "nameko run evaluation.evaluation.service" in tab 1 of window 1' -e 'delay 1'
fi