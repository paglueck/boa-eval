from nameko.rpc import rpc, RpcProxy
#from eval import ?

'''
Constant vars - Necessary microservice names
'''
CONST_SERVICE_NAME = 'evaluation_service'
CONST_SERVICE_NAME_GATEWAY = 'gateway_service'


class EvaluationService:
    """
    Microservice offering the evaluation
    """

    '''
    Name the microservice reacts to, when listening for calls
    '''
    name = CONST_SERVICE_NAME

    '''
    RPC proxies the anomaly detection service needs to call other services it depends on
    '''
    gateway = RpcProxy(CONST_SERVICE_NAME_GATEWAY)

    @rpc
    def eval(self):
        # Call eval class or whatever

        # Granularity for the logs - use whole day, split in half a day, quarter a day (DAY, HALFDAY, QUARTERDAY)
        granularity = 'DAY'

        # Known useful actions for now: LOGIN, LOGOUT, PW-CHANGE, EMAIL-CHANGE, PW-RESET
        action_dist = {"LOGIN": 0.7, "LOGOUT": 0.2, "PW-CHANGE": 0.1}

        # Defined anomalies for now: 1 = one occurrence of that anomaly type
        # anomalies = {"brute": 1, "pw": 1, "mail": 1}

        # Parameter for detection (alpha = sensitivity, higher = more anomalies, lower = less anomalies)
        alpha = 0.65

        # Training data without anomalies
        training_behavior = {"days": 7,
                             "actions_amount": 20,
                             "action_distribution": action_dist,
                             "anomalies": {},#anomalies,
                             "day_offset": 0,
                             "minutes_to_distribute": 1300,
                             "deviation_lower": -2,
                             "deviation_upper": 2}

        # Defined anomalies for now: 1 = one occurrence of that anomaly type (more than 1 per anomaly possible)
        anomalies = {"brute": 1} #, "pw": 1, "mail": 1}

        # Prediction data with anomalies
        prediction_behavior = {"days": 1,
                               "actions_amount": 20,
                               "action_distribution": action_dist,
                               "anomalies": anomalies,
                               "day_offset": 0,
                               "minutes_to_distribute": 1300,
                               "deviation_lower": -2,
                               "deviation_upper": 2}

        args = {'granularity': granularity,
                'training_behavior': training_behavior,
                'predict_behavior': prediction_behavior,
                'alpha': alpha}

        result = self.gateway.generate_behavior_and_detect(args)

        return result[2]
