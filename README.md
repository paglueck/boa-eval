---------------------------------------------------------------------------
#Evaluation Framework for the Bag of Actions approach with the log generator
---------------------------------------------------------------------------

## Setup

To run the framework there needs to be a running RabbitMQ Server (using Version 3.7.6) on http://localhost:5672

There's three options to get a running RabbitMQ-Server quickly:

- Create a Docker container with the RabbitMQ image from Docker Hub
- Install RabbitMQ via Homebrew (```brew update``` > ```brew install rabbitmq```) and start with '/usr/local/sbin/rabbitmq-server'
- Download it from RabbitMQ (https://www.rabbitmq.com/download.html)

Run ```bash start.sh``` - (Mac only) This will create a new virtual environment and installs the required packages if it doesn't exist yet.
If the virtual environment exists already the script will only start the microservices.

