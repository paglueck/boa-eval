import hashlib

CONST_RID_LENGTH = 15
CONST_ACTION_LOGIN = "LOGIN"
CONST_ACTION_LOGOUT = "LOGOUT"
CONST_ACTION_PWCHANGE = "PW-CHANGE"
CONST_ACTION_EMAILCHANGE = "EMAIL-CHANGE"
CONST_ACTION_FAILEDLOGIN = "LOGINFAIL"


class Action:

    def __init__(self, timestamp, ip, uid, desc):
        self.timestamp = timestamp
        self.ip = ip
        self.uid = uid
        self.status = 200
        self.szero = 0
        self.desc = desc

    def generate_request_id(self):
        return hashlib.md5(self.timestamp.isoformat() + self.ip + self.desc).hexdigest()[0:15]

    def __str__(self):
        return self.timestamp.isoformat()[0:23] + "Z" + " R{" + self.generate_request_id() + "} " + "IP{" + self.ip + "} " + "H{" + str(self.status) + "} " + "S{" + str(self.szero) + "} " + "U{" + self.uid + "} " + self.desc + " {}"


class Login(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_LOGIN)


class Logout(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_LOGOUT)


class Pwchange(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_PWCHANGE)


class EmailChange(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_PWCHANGE)


class Pwreset(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_PWCHANGE)


class FailedLogin(Action):

    def __init__(self, timestamp, ip, uid):
        Action.__init__(self, timestamp, ip, uid, CONST_ACTION_FAILEDLOGIN)

    #def __str__(self):
     #   return self.timestamp.isoformat()[0:23] + "Z" + " R{" + self.generate_request_id() + "} " + "IP{" + self.ip + "} " + "H{" + str(self.status) + "} " + "S{" + str(self.szero) + "} " + "U{" + self.uid + "} " + self.desc + " {'success': 'false'}"
