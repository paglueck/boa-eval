class User:

    def __init__(self, ip, id):
        self.ip = ip
        self.id = id
        #self.behavior = behavior

    def __str__(self):
        return "User with IP: " + self.ip + " and UID: " + self.id #+ "\n" + str(self.behavior)


class Behavior:

    def __init__(self, duration, entries_per_timeslice, action_distrib, anomalies):
        self.duration = duration
        self.entries_per_timeslice = entries_per_timeslice
        self.action_distrib = ActionDistribution(action_distrib)
        self.anomalies = anomalies

    def __str__(self):
        behaviorstr = "Behavior:\n" + str(self.duration) + " days\n" + str(self.entries_per_timeslice) + " entries per day\n"
        behaviorstr += "Anomalies: " + str(self.anomalies) + "\nDistribution of actions:\n" + str(self.action_distrib)
        return behaviorstr


class ActionDistribution:

    def __init__(self, dist):
        self.login = dist[0]
        self.logout = dist[1]
        self.flogin = dist[2]
        self.pwchange = dist[3]
        self.emailchange = dist[4]
        self.pwreset = dist[5]

    def __str__(self):
        return "Percentage logins: " + str(self.login) + "\nPercentage logouts: " + str(
            self.logout) + "\nPercentage pw changes: " + str(self.pwchange) #+ "\nPercentage email changes: " + str(
            #self.emailchange)
