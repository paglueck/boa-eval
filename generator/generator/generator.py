import hashlib
import random
from datetime import datetime, timedelta
from actions import Action
from user import User

'''
Const vars - Interval for log entry deviation, length of user id
'''
CONST_DEVIATION_UPPER = 4
CONST_DEVIATION_LOWER = 0
CONST_UID_LENGTH = 8

'''
Anomalies
'''
CONST_ANOMALIE_BRUTEFORCE = {"LOGINFAIL": 0.2}
CONST_ANOMALIE_MAILACCESS = {"PW-RESET": 0.1, "EMAIL-CHANGE": 0.1, "PW-CHANGE": 0.1}
CONST_ANOMALIE_PWCHANGES = {"PW-CHANGE": 0.2}


def generate_logs_for_ip_user(ip, days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper):
    """
    Generates user behavior logs with the given parameters

    Parameters
    ----------
    :param ip: IP of the user
    :param days: Amount of days of logs that should be generated
    :param entries: Amount of entries each log should have in total
    :param distribution: Distribution of different actions the user executes {"LOGINS": 0,7, ...}
    :param anomalies: Anomalies that should be injected in the logs {"LOGINFAIL": 1, "BRUTEF": 0,..}
    :param day_offset: Amount of days the timestamps should be moved in the future or past (positive=future,
    negative=past)
    :param minutes_to_distribute: Amount of total minutes the actions should be distributed over (1300 is a good value
    to not float into the next day with the actions)
    :param entry_deviation_lower: Lower bound for the deviation to the total entry amount
    :param entry_deviation_upper: Upper bound for the deviation to the total entry amount

    Return
    -------
    :return: Returns an array of arrays with the log data, where each of the arrays contains one day of log entries
    ([[log_entry,...],..[]])
    """

    '''
    Step 1 - Generate the user with the behavior parameters
    '''

    user = generate_user_with_ip(ip)

    '''
    Step 2 - Randomly determine where to place the anomalie(s)
    '''
    anomaly_days = {}
    if anomalies:
        for anomaly in anomalies.keys():
            random_day = str(random.randint(0, days - 1))
            if random_day not in anomaly_days.keys():
                anomaly_days[random_day] = [anomaly]
            else:
                anomaly_days[random_day].append(anomaly)

    # TODO: Determine day to put in the anomalies

    '''
    Step 3 - Calculate amount and generate actions for each type
    '''
    logs = []

    # Get today's date and set it to midnight for the action timestamps
    action_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    # Offset the date into the past or future
    action_time += timedelta(days=day_offset)

    for day in range(days):

        actions = []

        '''
        Step 3.1 - Calculate amount of total entries + random deviation
        '''
        entries_random_deviation = random.randint(entry_deviation_lower, entry_deviation_upper)
        entries_total = entries + entries_random_deviation
        # TODO: Factor in the anomalies that potentially will be added

        '''
        Step 3.2 - Calculate amount of entries for each type and add to this day's actions
        '''
        for action_type in distribution:
            entries_for_type = int(distribution[action_type] * entries_total)
            for entry in range(entries_for_type):
                actions.append(Action("", user.ip, user.id, action_type))

        if str(day) in anomaly_days.keys():
            for anomaly in anomaly_days[str(day)]:
                anomaly_template = None
                if anomaly == "brute":
                    anomaly_template = CONST_ANOMALIE_BRUTEFORCE
                elif anomaly == "mail":
                    anomaly_template = CONST_ANOMALIE_MAILACCESS
                elif anomaly == "pw":
                    anomaly_template = CONST_ANOMALIE_PWCHANGES
                for action_type in anomaly_template.keys():
                    entries_for_type = int(anomaly_template[action_type] * entries_total)
                    for entry in range(entries_for_type):
                        actions.append(Action("", user.ip, user.id, action_type))

        '''
        Step 3.3 - Shuffle the array
        '''
        random.shuffle(actions)

        '''
        Step 3.4 - Calculate timestamps and add to the actions
        '''
        # Add current day as offset to change days
        timestamp_base = action_time + timedelta(days=day)

        # Calculate minute offset to distribute the actions only over this day
        action_time_offset = minutes_to_distribute / len(actions)

        # Give each action a timestamp and increment the time by the time offset
        for action in actions:
            action.timestamp = timestamp_base
            timestamp_base += timedelta(minutes=action_time_offset)

        # Replace each action with its string representation
        actions = map(lambda a: str(a), actions)

        # Add the new log entries to the log array
        logs.append(actions)

    return logs


def generate_logs_for_random_user(days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper):
    return generate_logs_for_ip_user(generate_ip(), days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper)


def generate_user_with_ip(ip):
    user_ip = ip
    user_id = generate_uid(user_ip)
    return User(user_ip, user_id)


def generate_ip():
    return str(random.randint(0, 255)) + "." + str(random.randint(0, 255)) + "." + str(
        random.randint(0, 255)) + "." + str(random.randint(0, 255))


def generate_uid(ip):
    return hashlib.md5(ip.encode('utf-8')).hexdigest()[0:CONST_UID_LENGTH]
