from nameko.rpc import rpc, RpcProxy
from . import generator

'''
Constant vars - Necessary microservice names
'''
CONST_SERVICE_NAME_GEN = 'generator_service'
CONST_SERVICE_NAME_HUB = 'hub_service'

'''
Constant vars - Example values for behavior generation
'''
CONST_TEST_USER_BEHAVIOR_DAYS = 7
CONST_TEST_USER_BEHAVIOR_ACTIONS = 20
CONST_TEST_USER_BEHAVIOR_ACTIONDIST = [0.7, 0.2, 0.0, 0.1, 0.0, 0.0]
CONST_TEST_USER_BEHAVIOR_ACTIONDIST2 = {'LOGIN': 0.7,
                                        'LOGOUT': 0.2,
                                        "PW-CHANGE": 0.1}
CONST_TEST_USER_BEHAVIOR_ANOMALIES = {"brute": 1, "pw": 1, "mail": 1}
CONST_TEST_USER_ACTION_TIME_OFFSET = 1300
CONST_TEST_USER_DEVIATION_UPPER = 4
CONST_TEST_USER_DEVIATION_LOWER = 0

class GeneratorService:
    """
    Microservice offering the generation of parametrized
    """

    '''
    Name the microservice reacts to, when listening for calls
    '''
    name = CONST_SERVICE_NAME_GEN

    '''
    RPC proxies the anomaly detection service needs to call other services it depends on
    '''
    hub = RpcProxy(CONST_SERVICE_NAME_HUB)

    @rpc
    def generate_with_args(self, days, actions_amount, action_distribution, anomalies, day_offset, minutes_to_distribute, deviation_lower, deviation_upper):
        """
        Generator RPC Endpoint - real functionality

        Parameters
        ----------
        :param days: The amount of days of logs that should be generated
        :param actions_amount: The amount of entries each log should have in total
        :param action_distribution: The distribution of different actions the user executes {"LOGINS": 0,7, ...}
        :param anomalies: The anomalies that should be injected in the logs {"LOGINFAIL": 1, "BRUTEF": 0,..}
        :param day_offset: Amount of days the timestamps should be moved in the future or past (positive=future,
        negative=past)
        :param minutes_to_distribute: Amount of total minutes the actions should be distributed over (1300 is a good value
        to not float into the next day with the actions)
        :param deviation_lower: Lower bound for the deviation to the total entry amount
        :param deviation_upper: Upper bound for the deviation to the total entry amount

        :return: Returns the array of arrays with the log data from the generator
        """
        logs = generator.generate_logs_for_random_user(days,
                                                       actions_amount,
                                                       action_distribution,
                                                       anomalies,
                                                       day_offset,
                                                       minutes_to_distribute,
                                                       deviation_lower,
                                                       deviation_upper)
        return logs

    @rpc
    def generate_with_default_args(self):
        """
        Generator RPC Endpoint - preset args
        """
        logs = generator.generate_logs_for_random_user(CONST_TEST_USER_BEHAVIOR_DAYS,
                                                       CONST_TEST_USER_BEHAVIOR_ACTIONS,
                                                       CONST_TEST_USER_BEHAVIOR_ACTIONDIST2,
                                                       CONST_TEST_USER_BEHAVIOR_ANOMALIES,
                                                       0,
                                                       CONST_TEST_USER_ACTION_TIME_OFFSET,
                                                       CONST_TEST_USER_DEVIATION_LOWER,
                                                       CONST_TEST_USER_DEVIATION_UPPER)
        return logs

